//
//  KeyGeneration.h
//  VePass
//
//  Created by Hassan Shahbazi on 10/29/1395 AP.
//  Copyright © 1395 AP RSA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "x509.h"

@interface KeyGeneration : NSObject

@property (nonatomic, strong) x509 *certificateGenerator;
@property (nonatomic, strong) NSData *publicTag;
@property (nonatomic, strong) NSData *privateTag;

- (id)initWithPublicKeyHandler:(NSString *)publicKeyHandler
          andPrivateKeyHandler:(NSString *)privateKeyHandler;

- (BOOL)generateKeyPairPlease;

- (NSData *)getPublicKey;

- (NSData *)CreateX509Certificate;

- (NSData *)CreateSignature:(NSData *)data;

@end
