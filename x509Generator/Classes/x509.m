//
//  x.m
//  VePass
//
//  Created by boobi on 11/18/15.
//  Copyright © 2017 RSA. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "x509.h"
#include <openssl/conf.h>
#include <openssl/x509v3.h>
#include <openssl/engine.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/obj_mac.h>
#include <openssl/ec.h>
#include <openssl/sha.h>
#include <openssl/ecdh.h>
#include <openssl/evp.h>
#include <openssl/pkcs12.h>
#include <openssl/ecdsa.h>
#include <openssl/asn1.h>
#include <openssl/objects.h>
#include <openssl/bn.h>


#define PUBLICKEYLEN 65

@interface x509()
@property (nonatomic, assign) NSInteger lastPosition;
@end

@implementation x509 : NSObject

unsigned char * bufPUB;
EVP_PKEY * pkey;
EC_KEY *eckey;
EC_GROUP *ec_group;

- (id)init {
    self = [super init];
    if (self) {
        ERR_load_crypto_strings();
        OpenSSL_add_all_algorithms();
    }
    return self;
}

- (BOOL )setPrivateKey:(NSData *)privateKey {
    const unsigned char *privateKeyBits = (unsigned char *) [privateKey bytes];
    
    ec_group = EC_GROUP_new_by_curve_name(NID_X9_62_prime256v1);
    eckey = EC_KEY_new();
    EC_KEY_set_group(eckey, ec_group);
    EC_KEY_generate_key(eckey);
    EC_KEY_set_asn1_flag(eckey, OPENSSL_EC_NAMED_CURVE);

    if (privateKeyBits != NULL) {
        BIGNUM *prv = BN_bin2bn(privateKeyBits, sizeof(privateKeyBits), NULL);
        EC_KEY_set_private_key(eckey, prv);
        
        EC_POINT *pub = EC_POINT_new(ec_group);
        EC_POINT_mul(ec_group, pub, prv, NULL, NULL, NULL);
        EC_KEY_set_public_key(eckey, pub);
        
        pkey = EVP_PKEY_new();
        EVP_PKEY_set1_EC_KEY(pkey, eckey);
        [self createBuffer];
        return true;
    }
    return false;
}

- (void)createBuffer {
    int lenPub = i2o_ECPublicKey(eckey, NULL); //find out required buffer
    bufPUB = (unsigned char*) malloc(lenPub);
    unsigned char *pubKey2 = bufPUB;
    i2o_ECPublicKey(eckey, &pubKey2);
}

- (NSData *) getPublicKey {
    return [NSData dataWithBytes:bufPUB length:PUBLICKEYLEN];
}

- (NSData *) generateCertificate {
    
    X509 * x509;
    x509 = X509_new();
    
    X509_set_version(x509,2);
    ASN1_INTEGER_set(X509_get_serialNumber(x509), 1);
    
    
    X509_gmtime_adj(X509_get_notBefore(x509), 0);
    X509_gmtime_adj(X509_get_notAfter(x509), 31536000L);
    
    X509_set_pubkey(x509, pkey);
    
    X509_NAME * name;
    name = X509_get_subject_name(x509);
    
    X509_NAME_add_entry_by_txt(name, "C",  MBSTRING_ASC,(unsigned char *)"CA", -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "O",  MBSTRING_ASC,(unsigned char *)"Vancosys Data Security Inc.", -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC,(unsigned char *)"Vancosys", -1, -1, 0);
    X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC,(unsigned char *)"Vancosys", -1, -1, 0);
    
    X509_set_issuer_name(x509, name);
    
    X509_sign(x509, pkey, EVP_sha256());
    
    int len = i2d_X509(x509, NULL); //find out required buffer
    unsigned char *bufCert= (unsigned char*) malloc(len);
    unsigned char * bufCert2 = bufCert;
    i2d_X509(x509, &bufCert2);
    NSData *result = [NSData dataWithBytes:(const void *)bufCert length:sizeof(unsigned char)*len];
    
    return result;
    
}

- (NSData *)generateSignature:(NSData *)data {
    uint8_t digest[32];
    ECDSA_SIG *signature;
    uint8_t *der, *der_copy;
    size_t der_len;
    
    NSMutableData * plainData = [[NSMutableData alloc] init];    
    [plainData appendBytes:data.bytes length:data.length];
    
    bbp_sha256(digest, (uint8_t *)plainData.bytes, plainData.length);
    signature = ECDSA_do_sign(digest, sizeof(digest), eckey);
    der_len = ECDSA_size(eckey);
    der = calloc(der_len, sizeof(uint8_t));
    der_copy = der;
    i2d_ECDSA_SIG(signature, &der_copy);
    
    return [NSData dataWithBytes:der length:der_len];
}

void bbp_sha256(uint8_t *digest, const uint8_t *message, size_t len) {
    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, message, len);
    SHA256_Final(digest, &ctx);
}

- (NSInteger)ConvertDataToInt:(NSData *)data {
    NSData *dt = [data subdataWithRange:NSMakeRange(0, [data length])];
    NSInteger len = 0;
    [dt getBytes:&len length:sizeof(int)];
    
    return len;
}

@end

