//
//  EDCH.h
//  x509Generator
//
//  Created by Hassan Shahbazi on 2018-06-24.
//  Copyright © 2018 Hassan Shahbazi. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/ecdh.h>
#include <openssl/ec.h>
#include <openssl/pem.h>


@interface ECDH : NSObject

- (EVP_PKEY *)generateKey;

- (NSData *)getPublicKey:(EVP_PKEY *)privateKey;

- (NSData *)deriveShared:(NSData *)publicKey pvKey:(EVP_PKEY *)privateKey;

@end
