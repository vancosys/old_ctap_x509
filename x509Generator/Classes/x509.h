//
//  x509.h
//  VePass
//
//  Created by boobi on 11/18/15.
//  Copyright © 2017 RSA. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface x509 : NSObject

- (BOOL )setPrivateKey:(NSData *)privat;

- (NSData *)getPublicKey;

- (NSData *)generateCertificate;

- (NSData *)generateSignature:(NSData *)data;
    
@end
