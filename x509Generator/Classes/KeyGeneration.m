//
//  KeyGeneration.m
//  VePass
//
//  Created by Hassan Shahbazi on 10/29/1395 AP.
//  Copyright © 1395 AP RSA. All rights reserved.
//

#import "KeyGeneration.h"
#import <Security/Security.h>
#import <CommonCrypto/CommonCrypto.h>

@implementation KeyGeneration

const size_t BUFFER_SIZE = 64;
const size_t CIPHER_BUFFER_SIZE = 1024;
const uint32_t PADDING = kSecPaddingNone;

SecKeyRef publicKey;
SecKeyRef privateKey;

- (id)initWithPublicKeyHandler:(NSString *)publicKeyHandler
          andPrivateKeyHandler:(NSString *)privateKeyHandler {
    self = [super init];
    if (self) {
        _publicTag = [publicKeyHandler dataUsingEncoding:NSUTF8StringEncoding];
        _privateTag = [privateKeyHandler dataUsingEncoding:NSUTF8StringEncoding];
    }
    return self;
}

- (BOOL)generateKeyPairPlease {
    OSStatus status = noErr;
    NSMutableDictionary *privateKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *publicKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *keyPairAttr = [[NSMutableDictionary alloc] init];
    
    publicKey = NULL;
    privateKey = NULL;
    
    [keyPairAttr setObject:(id)kSecAttrKeyTypeEC
                    forKey:(id)kSecAttrKeyType];
    
    [keyPairAttr setObject:[NSNumber numberWithInt:256]
                    forKey:(id)kSecAttrKeySizeInBits];
    
    [privateKeyAttr setObject:[NSNumber numberWithBool:YES] //keychain auto save
                       forKey:(id)kSecAttrIsPermanent];
    
    [privateKeyAttr setObject:(id)kSecAttrAccessibleAfterFirstUnlock
                       forKey:(id)kSecAttrAccessible];

    [privateKeyAttr setObject:_privateTag
                       forKey:(id)kSecAttrApplicationTag];
    
    [publicKeyAttr setObject:[NSNumber numberWithBool:YES] //keychain auto save
                      forKey:(id)kSecAttrIsPermanent];
    
    [publicKeyAttr setObject:(id)kSecAttrAccessibleAfterFirstUnlock
                       forKey:(id)kSecAttrAccessible];
    
    [publicKeyAttr setObject:_publicTag
                      forKey:(id)kSecAttrApplicationTag];
    
    [keyPairAttr setObject:privateKeyAttr
                    forKey:(id)kSecPrivateKeyAttrs];
    
    [keyPairAttr setObject:publicKeyAttr
                    forKey:(id)kSecPublicKeyAttrs];
    
    status = SecKeyGeneratePair((CFDictionaryRef)keyPairAttr,
                                &publicKey, &privateKey);
    if (status == noErr) {
        return YES;
    }
    return NO;
}

- (SecKeyRef)getPublicKeyRef {
    OSStatus resultCode = noErr;
    SecKeyRef publicKeyReference = NULL;
    
    if(publicKey == NULL) {
        NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
        
        // Set the public key query dictionary.
        [queryPublicKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
        [queryPublicKey setObject:_publicTag forKey:(id)kSecAttrApplicationTag];
        [queryPublicKey setObject:(id)kSecAttrKeyTypeEC forKey:(id)kSecAttrKeyType];
        [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnRef];
        [queryPublicKey setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];

        // Get the key.
        resultCode = SecItemCopyMatching((CFDictionaryRef)queryPublicKey, (CFTypeRef *)&publicKeyReference);
        
        if(resultCode != noErr) {
            publicKeyReference = NULL;
        }
    } else {
        publicKeyReference = publicKey;
    }
    
    return publicKeyReference;
}

- (NSData *)getPublicKeyBits {
    OSStatus sanityCheck = noErr;
    NSData * publicKeyBits = nil;
    
    NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
    
    // Set the public key query dictionary.

    [queryPublicKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [queryPublicKey setObject:_publicTag forKey:(id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(id)kSecAttrKeyTypeEC forKey:(id)kSecAttrKeyType];
    [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnData];
    [queryPublicKey setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];
    
    // Get the key bits.
    sanityCheck = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, (void *)&publicKeyBits);
    
    if (sanityCheck != noErr) {
        publicKeyBits = nil;
    }
    else if (sanityCheck == errSecItemNotFound) {
        publicKeyBits = nil;
    }
    
    return publicKeyBits;
}

- (SecKeyRef)getPrivateKeyRef {
    OSStatus resultCode = noErr;
    SecKeyRef privateKeyReference = NULL;
    
    if(privateKey == NULL) {
        NSMutableDictionary * queryPrivateKey = [[NSMutableDictionary alloc] init];
        
        // Set the private key query dictionary.
        [queryPrivateKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
        [queryPrivateKey setObject:_privateTag forKey:(id)kSecAttrApplicationTag];
        [queryPrivateKey setObject:(id)kSecAttrKeyTypeEC forKey:(id)kSecAttrKeyType];
        [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnRef];
        [queryPrivateKey setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];
        
        // Get the key.
        resultCode = SecItemCopyMatching((CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&privateKeyReference);

        if(resultCode != noErr) {
            privateKeyReference = NULL;
        }
    } else {
        privateKeyReference = privateKey;
    }
    
    return privateKeyReference;
}

- (NSData *)getPrivateKeyBits {
    OSStatus sanityCheck = noErr;
    NSData * privateKeyBits = nil;
    
    NSMutableDictionary * queryPrivateKey = [[NSMutableDictionary alloc] init];
    
    // Set the public key query dictionary.
    [queryPrivateKey setObject:(id)kSecClassKey forKey:(id)kSecClass];
    [queryPrivateKey setObject:_privateTag forKey:(id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(id)kSecAttrKeyTypeEC forKey:(id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(id)kSecReturnData];
    [queryPrivateKey setObject:(id)kSecAttrAccessibleAfterFirstUnlock forKey:(id)kSecAttrAccessible];
    
    // Get the key bits.
    sanityCheck = SecItemCopyMatching((__bridge CFDictionaryRef)queryPrivateKey, (void *)&privateKeyBits);
    
    if (sanityCheck != noErr) {
        privateKeyBits = nil;
    }
    else if (sanityCheck == errSecItemNotFound) {
        privateKeyBits = nil;
    }
    
    return privateKeyBits;
}

- (BOOL )setKeyPair {
    _certificateGenerator = [[x509 alloc] init];
    return [_certificateGenerator setPrivateKey:[self getPrivateKeyBits]];
}

- (NSData *)getPublicKey {
    if ([self setKeyPair])
        return [_certificateGenerator getPublicKey];
    return [NSData new];
}

- (NSData *)CreateX509Certificate {
    if ([self setKeyPair]) {
        return [_certificateGenerator generateCertificate];
    }
    return [NSData new];
}


- (NSData *)CreateSignature:(NSData *)data {
    if ([self setKeyPair])
        return  [_certificateGenerator generateSignature:data];
    return [NSData new];
}



@end
